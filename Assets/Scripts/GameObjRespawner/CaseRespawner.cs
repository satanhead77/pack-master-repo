﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaseRespawner : BaseGameObjRespawner
{
	private GameObject _respawnedCase;

	private void Start()
	{
		Main.Instance.OnTriggerTable.PlayerCameToTheTableEvent += RespawnCase;
	}

	private void OnDestroy()
	{
		Main.Instance.OnTriggerTable.PlayerCameToTheTableEvent -= RespawnCase;
	}

	private void RespawnCase()
	{
		_respawnedCase = RespawnObject(SwapgameObjectsList());
		Main.Instance.CaseMoveController.SetEnabledCase(_respawnedCase);
	}

	private void DestroyCase()
	{

	}
}
