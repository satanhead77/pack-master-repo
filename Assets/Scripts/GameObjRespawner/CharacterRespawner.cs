﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterRespawner : BaseGameObjRespawner
{
	[SerializeField] private List<GameObject> _respawnedCharactersList;
	[SerializeField] private int _respawnedCharactersCount;
	[SerializeField] private int _maximumNumberOfCharacters;
	[SerializeField] private float _respawnTimer;
	private float _respawnTimerStartValue;

	private void Start()
	{
		_respawnTimerStartValue = _respawnTimer;
		Main.Instance.OnTriggerDestroy.DestroyCharacterEvent += RemoveCharacter;
	}

	private void OnDestroy()
	{
		Main.Instance.OnTriggerDestroy.DestroyCharacterEvent -= RemoveCharacter;
	}

	private void Update()
	{
		RespawnCharacter();
	}

	private void RespawnCharacter()
	{
		if (_respawnedCharactersCount < _maximumNumberOfCharacters)
		{
			_respawnTimer -= Time.deltaTime;
			if (_respawnTimer <= 0)
			{
				var respawnedObj = RespawnObject(SwapgameObjectsList());
				_respawnedCharactersList.Add(respawnedObj);
				_respawnedCharactersCount++;
				_respawnTimer = _respawnTimerStartValue;
			}
		}
	}

	private void RemoveCharacter(GameObject character)
	{
		_respawnedCharactersList.Remove(character);
		_respawnedCharactersCount--;
	}
}
