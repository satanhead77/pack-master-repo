﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseGameObjRespawner : BaseObject
{
    [SerializeField] protected List<GameObject> _gameObjectsToRespawn;
	[SerializeField] protected Transform _respawnPosition;

	protected GameObject RespawnObject(GameObject currentGo)
	{
		var gameObj = BaseObjectInstantiate(currentGo, _respawnPosition.position, Quaternion.identity);
		return gameObj;
	}

	protected GameObject SwapgameObjectsList()// будет выдавать рандомные объекты, для этого надо перемешатьрандомно список 
	{
		System.Random random = new System.Random();

		for (int i = _gameObjectsToRespawn.Count - 1; i >= 1; i--)
		{
			int j = random.Next(i + 1);
			GameObject temp = _gameObjectsToRespawn[j];
			_gameObjectsToRespawn[j] = _gameObjectsToRespawn[i];
			_gameObjectsToRespawn[i] = temp;
		}

		return _gameObjectsToRespawn[0];
	}
}
