﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RespawnedCharacter : BaseObject
{
	public Transform moveTarget;

	public NavMeshAgent meshAgent;

	public Animator animator;

	private GameObject table;

	[SerializeField] private float _minDistanceToTarget;

	[SerializeField] private Canvas _characterCanvas;

	void Start()
    {
		meshAgent = GetComponent<NavMeshAgent>();
		animator = GetComponent<Animator>();
		table = GameObject.FindGameObjectWithTag("CharacterTarget");
		moveTarget = table.transform;
		_characterCanvas.enabled = false;
		AddCharacterToList();
	}



	public void SetMoveTarget()
	{
		int index = 0;

		for(int i = 0; i < Main.Instance.CharactersList.characters.Count; ++i)
		{
			if (Main.Instance.CharactersList.characters[i] == this)
			{
				index = i;

				if (index != 0)
				{
					moveTarget = Main.Instance.CharactersList.characters[i - 1].transform;
				}
				else
				{
					moveTarget = table.transform;
				}
			}
		}
	}

	public void SetMoveTarget(Transform target)
	{
		moveTarget = target;
	}

	public bool StopCharacter()
	{
		float distance = Vector3.Distance(moveTarget.position, Position);

		if (distance <= _minDistanceToTarget)
		{
			meshAgent.isStopped = true;
		}
		else
		{
			meshAgent.isStopped = false;
		}

		return meshAgent.isStopped;

	}

	private void AddCharacterToList()
	{
		Main.Instance.CharactersList.AddCharacterToList(this);
	}

	private void RemoveCharacterFromList()
	{
		Main.Instance.CharactersList.RemoveCharacterFromList(this);
	}

	private void OnDestroy()
	{
		RemoveCharacterFromList();
	}
}
