﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Suitcase Settings", menuName = "Suitcase Settings", order = 51)]
public class SuitcaseSettings : ScriptableObject
{
	public int puzzleFieldSize;

	public Item[] itemsArray;

	public int minDiamondsCount;

	public int maxDiamondsCount;// максимальное и  минимальное кол-во денег которое можно получить за закрытие кейса(выйгрыш в пазле)
}
