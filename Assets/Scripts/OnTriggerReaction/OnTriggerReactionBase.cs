﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class OnTriggerReactionBase : MonoBehaviour
{
	[SerializeField] protected string _characterTag;

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == _characterTag)
		{
			OnTriggerReaction(other.gameObject);
		}
	}

	protected abstract void OnTriggerReaction(GameObject gameObject);
}
