﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTriggerDestroy : OnTriggerReactionBase
{
	public event Action<GameObject> DestroyCharacterEvent;

	protected override void OnTriggerReaction(GameObject gameObject)
	{
		DestroyCharacterEvent?.Invoke(gameObject);
		Destroy(gameObject);
	}
}
