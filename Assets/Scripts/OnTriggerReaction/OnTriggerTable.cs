﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTriggerTable : OnTriggerReactionBase
{
	public event Action PlayerCameToTheTableEvent;

	[SerializeField] private bool _isWentToTheTable;

	private Canvas _characterCanvas;

	protected override void OnTriggerReaction(GameObject gameObject)
	{
		if (!_isWentToTheTable)
		{
			PlayerCameToTheTableEvent?.Invoke();

			_isWentToTheTable = true;

			var canvasTransform = gameObject.transform.Find("Canvas");

			_characterCanvas = canvasTransform.GetComponent<Canvas>();
			_characterCanvas.enabled = true;

			Debug.Log("TableTrigger");
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.gameObject.tag == _characterTag)
		{
			if (_characterCanvas!=null)
			{
				_characterCanvas.enabled = false;
			}

			_isWentToTheTable = false;
		}
	}
}
