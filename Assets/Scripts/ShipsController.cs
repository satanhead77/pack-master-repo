﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipsController : BaseController
{
	[SerializeField] private ShipFly[] _ships;

	public override void ControllerUpdate()
	{
		if (_controllerIsEnabled)
		{
			foreach (var ship in _ships)
			{
				ship.Fly();
			}
		}
	}
}
