﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CharacterSuitcase : MonoBehaviour
{
	public SuitcaseSettings suitcaseSettings;

	public int characterDiamondsCount;

	[SerializeField] private TextMeshProUGUI _diamondsCountText;


	private void Start()
	{
		GenerateCharacterDiamondsCount();

		_diamondsCountText.text = characterDiamondsCount.ToString();
	}

	private void GenerateCharacterDiamondsCount()
	{
		characterDiamondsCount = Random.Range(suitcaseSettings.minDiamondsCount, suitcaseSettings.maxDiamondsCount);
	}
}
