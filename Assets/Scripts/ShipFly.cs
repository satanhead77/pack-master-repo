﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipFly : BaseObject
{
	private float min;
	[SerializeField] private float max;

	void Start()
	{
		min = Position.y;
	}

	public void Fly()
	{
		Position = new Vector3(Position.x, Mathf.PingPong(Time.time * 0.4f, max - min) + min, Position.z);
	}
}
