﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DiamondsCounter : MonoBehaviour
{
	public event Action  DiamondsAdded;
	[SerializeField] private int _diamondsCount;

	private void Start()
	{
		AddDiamonds(0);// обновим при старте юи
	}

	public int GetDiamondsCount()
	{
		return _diamondsCount;
	}

	public void AddDiamonds(int diamondsCount)
	{
		_diamondsCount += diamondsCount;
		DiamondsAdded?.Invoke();
	}
}
