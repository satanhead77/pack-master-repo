﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharactersMoveController : BaseController
{
	[SerializeField] private float _rotateSpeed;
	 
	public override void ControllerUpdate()
	{
		if (_controllerIsEnabled)
		{
			Move();
			RotateToTarget();
		}
	}

	private void Move()
	{ 
		foreach (var character in Main.Instance.CharactersList.characters)
		{
			if (!character.StopCharacter())
			{
				character.SetMoveTarget();
				character.meshAgent.SetDestination(character.moveTarget.position);
			}
		}
	}

	private void RotateToTarget()
	{
		var character = Main.Instance.CharactersList.characters[0];

		if (character.StopCharacter())
			character.Rotation = Quaternion.RotateTowards(character.Rotation, Quaternion.LookRotation(Camera.main.transform.position), _rotateSpeed * Time.deltaTime);
	}

}
