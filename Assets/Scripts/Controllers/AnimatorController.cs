﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorController : BaseController
{
	public override void ControllerUpdate()
	{
		if(_controllerIsEnabled)
		AnimateCharacters();
	}

	private void AnimateCharacters()
	{
		foreach(var character in Main.Instance.CharactersList.characters)
		{
			if (!character.meshAgent.isStopped)
			{
				character.animator.SetBool("Move", true);
			}
			else
			{
				character.animator.SetBool("Move", false);
			}
		}

		if (Main.Instance.CharacterGoAway.GetCharacter() != null)
		{
			var character = Main.Instance.CharacterGoAway.GetCharacter();

			if (!character.meshAgent.isStopped)
			{
				character.animator.SetBool("Move", true);
			}

			else
			{
				character.animator.SetBool("Move", false);
			}
		}

	}
}
