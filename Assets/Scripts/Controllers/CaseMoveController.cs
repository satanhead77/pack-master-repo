﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaseMoveController : BaseController
{
	[SerializeField] private float _moveSpeed;
	[SerializeField] private Transform _moveTarget;
	[SerializeField] private Transform _backMoveTarget;
	[SerializeField] private Transform _winnerMoveTarget;
	[SerializeField] private GameObject _enabledCase;
	[SerializeField] private bool _isMovingBack;
	[SerializeField] private bool _isMovingForward;
	[SerializeField] private bool _isMovingToWinnerSide;

	private void Start()
	{
		CanvasManager.Instance.BaggageAcceptanceCanvas.SkipButtonCkicked += MoveBackEnable;
		Main.Instance.PuzzleGame.PlayerWin += PlayerWinHandler;
	}

	private void OnDestroy()
	{
		CanvasManager.Instance.BaggageAcceptanceCanvas.SkipButtonCkicked -= MoveBackEnable;
		Main.Instance.PuzzleGame.PlayerWin -= PlayerWinHandler;
	}

	public override void ControllerUpdate()
	{
		if (_controllerIsEnabled)
		{
			Move();
			MoveBack();
			MoveInWinnerSide();
		}
		 
	}

	public void SetEnabledCase(GameObject enabledCase)
	{
		_enabledCase = enabledCase;
		_isMovingForward = true;
		_isMovingBack = false;
		_isMovingToWinnerSide = false;
	}


	private void Move()
	{
		if (_isMovingForward)
		{
			if (_enabledCase != null)
				_enabledCase.transform.position = Vector3.MoveTowards(_enabledCase.transform.position, _moveTarget.position, _moveSpeed * Time.deltaTime);
		} 
	}

	private void MoveBack()
	{
		if (_isMovingBack)
		{
			if (_enabledCase != null)
				_enabledCase.transform.position = Vector3.MoveTowards(_enabledCase.transform.position, _backMoveTarget.position, _moveSpeed * Time.deltaTime);
		}
	}

	private void MoveInWinnerSide()
	{
		if (_isMovingToWinnerSide)
		{
			if (_enabledCase != null)
				_enabledCase.transform.position = Vector3.MoveTowards(_enabledCase.transform.position, _winnerMoveTarget.position, _moveSpeed * Time.deltaTime);
		}
	}

	private void MoveBackEnable()
	{
		_isMovingBack = true;
		_isMovingForward = false;
		_isMovingToWinnerSide = false;
	}

	private void PlayerWinHandler()
	{
		_isMovingToWinnerSide = true;
		_isMovingBack = false;
		_isMovingForward = false;
	}

	

}
