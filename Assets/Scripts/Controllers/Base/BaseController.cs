﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseController : BaseObject
{
	protected bool _controllerIsEnabled = true;

	public abstract void ControllerUpdate();

	public void EnableController()
	{
		_controllerIsEnabled = true;
	}

	public void DisableController()
	{
		_controllerIsEnabled = false;
	}
}
