﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseCanvas : BaseObject
{
	[SerializeField] protected Canvas _canvas;

	public void EnableCanvas()
	{
		_canvas.enabled = true;
	}

	public void DisableCanvas()
	{
		_canvas.enabled = false;
	}

	protected void DisableOrEnableObject(GameObject gameObject, bool value)
	{
		gameObject.SetActive(value);
	}

	protected void DisableOrEnableObject(MonoBehaviour monoBehaviour, bool value)
	{
		monoBehaviour.enabled = value;
	}
}
