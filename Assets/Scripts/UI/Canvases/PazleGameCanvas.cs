﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class PazleGameCanvas : BaseCanvas////опечатка
{
	public event Action GoButtonCliced;
	public event Action CanvasEnabled;
	public event Action CanvasDisabled;

	[SerializeField] bool _canvasIsEnabled;
	[SerializeField] private GameObject _goButton;
	[SerializeField] private Image _crossImage;
	[SerializeField] private Image _yesTicImage;

	private void Start()
	{
		CanvasManager.Instance.BaggageAcceptanceCanvas.AcceptButtonCliced += EnableCanvas;
		Main.Instance.PuzzleGame.AllItemsAreOccupied += AllItemsAreOccupied;
		Main.Instance.PuzzleGame.NotAllItemsAreOccupied += NotAllItemsAreOccupied;
		Main.Instance.PuzzleGame.PlayerLost += PlayerLostHandler;
		Main.Instance.PuzzleGame.PlayerWin += PlayerWinHandler;
	}

	private void Update()
	{
		if (_canvas.enabled)
		{
			if (!_canvasIsEnabled)
			{
				CanvasEnabled?.Invoke();
				_canvasIsEnabled = true;
			}
		}

		if (!_canvas.enabled)
		{
			if (_canvasIsEnabled)
			{
				CanvasDisabled?.Invoke();
				DisableOrEnableObject(_crossImage, false);
				DisableOrEnableObject(_goButton, false);
				DisableOrEnableObject(_yesTicImage, false);
				_canvasIsEnabled = false;
			}
		}
	}

	private void OnEnable()
	{
		DisableOrEnableObject(_goButton, false);
		DisableOrEnableObject(_crossImage, false);
		DisableOrEnableObject(_yesTicImage, false);
	}

	private void OnDestroy()
	{
		CanvasManager.Instance.BaggageAcceptanceCanvas.AcceptButtonCliced -= EnableCanvas;
		Main.Instance.PuzzleGame.AllItemsAreOccupied -= AllItemsAreOccupied;
		Main.Instance.PuzzleGame.NotAllItemsAreOccupied -= NotAllItemsAreOccupied;
		Main.Instance.PuzzleGame.PlayerLost -= PlayerLostHandler;
		Main.Instance.PuzzleGame.PlayerWin -= PlayerWinHandler;
	}

	public void GoButtonCkick()
	{
		GoButtonCliced?.Invoke();
	}

	IEnumerator CanvasDisablePlayerLost()
	{
		DisableOrEnableObject(_crossImage, true);
		yield return new WaitForSeconds(0.3f);
		DisableCanvas();
	}

	private void PlayerLostHandler()
	{
		StartCoroutine(CanvasDisablePlayerLost());
	}

	IEnumerator CanvasDisablePlayerWin()
	{
		DisableOrEnableObject(_yesTicImage, true);
		yield return new WaitForSeconds(0.3f);
		DisableCanvas();
	}

	private void PlayerWinHandler()
	{
		StartCoroutine(CanvasDisablePlayerWin());
	}

	private void AllItemsAreOccupied()
	{
		DisableOrEnableObject(_goButton, true);
	}

	private void NotAllItemsAreOccupied()
	{
		DisableOrEnableObject(_goButton, false);
	}
}
