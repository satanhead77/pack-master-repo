﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DiamondCanvas : BaseCanvas
{
	[SerializeField] private TextMeshProUGUI _diamondsCountText;

	private void Start()
	{
		Main.Instance.DiamondsCounter.DiamondsAdded += UpdateDiamondsCountText;
	}

	private void OnDestroy()
	{
		Main.Instance.DiamondsCounter.DiamondsAdded -= UpdateDiamondsCountText;
	}

	private void UpdateDiamondsCountText()
	{
		_diamondsCountText.text = Main.Instance.DiamondsCounter.GetDiamondsCount().ToString();
	}
}
