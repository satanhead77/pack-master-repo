﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BaggageAcceptanceCanvas : BaseCanvas
{
	public event Action SkipButtonCkicked;
	public event Action AcceptButtonCliced;

	private void Start()
	{
		Main.Instance.OnTriggerTable.PlayerCameToTheTableEvent += EnableCanvas;
		Main.Instance.PuzzleGame.PlayerLost += SkipCharacter;
	}

	private void OnDestroy()
	{
		Main.Instance.OnTriggerTable.PlayerCameToTheTableEvent -= EnableCanvas;
		Main.Instance.PuzzleGame.PlayerLost -= SkipCharacter;
	}

	public void AcceptCharacterCaseButtonClick()
	{
		AcceptButtonCliced?.Invoke();
	}

	public void SkipCharacterButtonCkick()
	{
		SkipCharacter();
	}

	private void SkipCharacter()
	{
		SkipButtonCkicked?.Invoke();
		DisableCanvas();
	}
}
