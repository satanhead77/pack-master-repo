﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasManager : MonoBehaviour
{
	[SerializeField] private BaggageAcceptanceCanvas _baggageAcceptanceCanvas;
	[SerializeField] private PazleGameCanvas _pazleGameCanvas;

	public BaggageAcceptanceCanvas BaggageAcceptanceCanvas { get { return _baggageAcceptanceCanvas; } }

	public PazleGameCanvas PazleGameCanvas { get { return _pazleGameCanvas; } }

	public static CanvasManager Instance { get; private set; }


	private void Awake()
	{
		Instance = this;

		_baggageAcceptanceCanvas.DisableCanvas();
		_pazleGameCanvas.DisableCanvas();
	}
}
