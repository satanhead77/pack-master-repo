﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Cell : MonoBehaviour, IDropHandler
{
	public bool _cellIsOccupied;

	private RectTransform _rectTransform;

	private Item _item;

	private void Start()
	{
		_rectTransform = GetComponent<RectTransform>();
	}

	private void Update()
	{
		if (_item != null)
		{
			if (!_item.isOncell)
			{
				_cellIsOccupied = false;
				_item = null;
			}
		}
	}

	public void OnDrop(PointerEventData eventData)
	{
		if(eventData.pointerDrag != null)
		{
			if (!_cellIsOccupied)
			{
				var dropedObj = eventData.pointerDrag;
				dropedObj.transform.SetParent(transform);
				dropedObj.transform.position = transform.position;//
				_item = dropedObj.GetComponent<Item>();
				_item.isOncell = true;
				_cellIsOccupied = true;
			}
		}
		 
	}
}
