﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridScaler : MonoBehaviour
{
	private GridLayoutGroup _grid;
	[SerializeField] private GridLayoutGroup _itemsGrid;
	private RectOffset _gridPadding;
	private RectTransform _parent;

	 
	private int _fieldSize;
	public float spacing;
	private float _spacingStartValue;

	Vector2 lastSize;

	void Start()
	{
		_spacingStartValue = spacing;
		_grid = GetComponent<GridLayoutGroup>();
		_grid.spacing = new Vector2(spacing, spacing);
		_parent = GetComponent<RectTransform>();
		_gridPadding = _grid.padding;
		lastSize = Vector2.zero;
	}

	void Update()
	{
		GridScale();
	}

	private void GridScale()
	{
		_fieldSize = _grid.constraintCount;

		if (_fieldSize > 3)
		{
			spacing = -12;
			_grid.spacing = new Vector2(spacing, spacing);
		}
		else
		{
			spacing = _spacingStartValue;
			_grid.spacing = new Vector2(spacing, spacing);
		}

		if (lastSize == _parent.rect.size)
		{
			return;
		}

		var paddingX = _gridPadding.left + _gridPadding.right;
		var cellSize = Mathf.Round((_parent.rect.width - paddingX - (_fieldSize - 1) * spacing) / _fieldSize);
		_grid.cellSize = new Vector2(cellSize, cellSize);
		_itemsGrid.cellSize = new Vector2(cellSize, cellSize);
	}
}
