﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PuzzleGame : MonoBehaviour 
{
	public event Action AllItemsAreOccupied;
	public event Action NotAllItemsAreOccupied;
	public event Action PlayerLost;
	public event Action PlayerWin;

	private SuitcaseSettings _suitcaseSettings;
	private CharacterSuitcase _characterSuitcase;
	private PuzzleGameField _puzzleGameField;//

	[SerializeField] private Cell _cellPrefab;
	[SerializeField] private List<Item> _repawnedItemsList;
	[SerializeField] private Transform _panel;

	[SerializeField] private Cell[,] _leftCellField;
	[SerializeField] private Cell[,] _rightCellField;
    [SerializeField] private GridLayoutGroup _gridLeftLayoutGroup = null;
	[SerializeField] private GridLayoutGroup _gridRightLayoutGroup = null;
	[SerializeField] private GridLayoutGroup _itemsLayoutGroup = null;
	[SerializeField] private bool _canvasIsEnabled;


	private void Start()
	{
		CanvasManager.Instance.PazleGameCanvas.GoButtonCliced += GoButtonClickHandler;
		CanvasManager.Instance.PazleGameCanvas.CanvasEnabled += InitGameField;
		CanvasManager.Instance.PazleGameCanvas.CanvasDisabled += ClearGameField;
	}

	private void OnDestroy()
	{
		CanvasManager.Instance.PazleGameCanvas.GoButtonCliced -= GoButtonClickHandler;
		CanvasManager.Instance.PazleGameCanvas.CanvasEnabled -= InitGameField;
		CanvasManager.Instance.PazleGameCanvas.CanvasDisabled -= ClearGameField;
	}

	private void Update()
	{
		if (_canvasIsEnabled)
		{ 
		  FreeItemsCounter();// временно 
		}
	}

	private void InitGameField()
	{
		GetLevelSettings();
		_repawnedItemsList = new List<Item>();
		_leftCellField = new Cell[_suitcaseSettings.puzzleFieldSize, _suitcaseSettings.puzzleFieldSize];
		_rightCellField = new Cell[_suitcaseSettings.puzzleFieldSize, _suitcaseSettings.puzzleFieldSize];

		_puzzleGameField = new PuzzleGameField(_suitcaseSettings, _cellPrefab);//
		_puzzleGameField.CreateGameField(_gridLeftLayoutGroup, _leftCellField);//
		_puzzleGameField.CreateGameField(_gridRightLayoutGroup, _rightCellField);//
		_puzzleGameField.CreateItems(_itemsLayoutGroup, _repawnedItemsList);//

		_canvasIsEnabled = true;
	}

	private void GetLevelSettings()
	{
		_characterSuitcase = Main.Instance.CharactersList.characters[0].GetComponent<CharacterSuitcase>();
		_suitcaseSettings = _characterSuitcase.suitcaseSettings;
	}

	private void ClearGameField()
	{
		foreach(var item in _repawnedItemsList)
		{
			_repawnedItemsList.Remove(item);
			Destroy(item.gameObject);
			Debug.Log("clearItems");
		}

		for(int i = 0; i < _suitcaseSettings.puzzleFieldSize; ++i)
		{

			for (int j = 0; j <_suitcaseSettings.puzzleFieldSize; ++j)
			{
				Destroy(_leftCellField[i, j].gameObject);
				_leftCellField[i, j] = null;
				Destroy(_rightCellField[i, j].gameObject);
				_rightCellField[i, j] = null;

				Debug.Log("clear");
			}
		}

		_canvasIsEnabled = false;

	}

	
	private void GoButtonClickHandler()
	{
		if(_puzzleGameField.FindAMirrorMatch(_leftCellField, _rightCellField))
		{
			PlayerLost?.Invoke();
			Debug.Log("проиграл");
		}
		else
		{
			PlayerWin?.Invoke();
			Main.Instance.DiamondsCounter.AddDiamonds(_characterSuitcase.characterDiamondsCount);
		}
	}

	private void FreeItemsCounter()
	{
		int count = 0;

		for(int i = 0; i < _repawnedItemsList.Count; ++i)
		{
			if (!_repawnedItemsList[i].isOncell)
			{
				++count;
			}
		}

		if (count == 0)
		{
			Debug.Log("нет свободных итемов");
			AllItemsAreOccupied?.Invoke();//
		}
		else
		{
			NotAllItemsAreOccupied?.Invoke();
		}

	}

}
