﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PuzzleGameField 
{
	private SuitcaseSettings _suitcaseSettings;

	private Cell _cellPrefab;

	public PuzzleGameField(SuitcaseSettings suitcaseSettings, Cell cellPrefab)
	{
		_suitcaseSettings = suitcaseSettings;
		_cellPrefab = cellPrefab;
	}

	public void CreateGameField(GridLayoutGroup gridLayoutGroup, Cell[,] cells)
	{
		if (gridLayoutGroup == null)
		{
			return;
		}

		gridLayoutGroup.constraintCount = _suitcaseSettings.puzzleFieldSize;

		var parentTransform = gridLayoutGroup.transform;

		for (var i = 0; i < _suitcaseSettings.puzzleFieldSize; ++i)
		{
			for (var j = 0; j < _suitcaseSettings.puzzleFieldSize; ++j)
			{
				var cellView = GameObject.Instantiate(_cellPrefab, parentTransform, true);
				cells[i, j] = cellView;
				Debug.Log(cells[i, j]);
			}
		}
	}


	public bool FindAMirrorMatch(Cell[,] arr1, Cell[,] arr2) //поиск зеркальных совпадений двух массивов, если оно есть то игрок проиграл 
	{
		for (int i = 0; i < _suitcaseSettings.puzzleFieldSize; ++i)
		{
			for (int j = 0; j < _suitcaseSettings.puzzleFieldSize; ++j)
			{
				if (arr1[i, j]._cellIsOccupied)
				{
					if (arr1[i, j]._cellIsOccupied && arr2[i, (_suitcaseSettings.puzzleFieldSize - 1) - j]._cellIsOccupied)
					{
						Debug.Log("совпадение");
						return true;
					}
				}

			}

		}

		return false;
	}

	public void CreateItems(GridLayoutGroup gridLayoutGroup,List<Item> _repawnedItemsList)
	{
		var parentTransform = gridLayoutGroup.transform;

		foreach (var item in _suitcaseSettings.itemsArray)
		{
			var respawnedItem = GameObject.Instantiate(item, parentTransform, true);
			_repawnedItemsList.Add(respawnedItem);
		}
	}

}
