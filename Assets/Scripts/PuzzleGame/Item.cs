﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Item : BaseObject, IBeginDragHandler, IEndDragHandler, IDragHandler
{
	public bool isOncell;

	public CanvasGroup canvasGroup;

	public Vector2 startPos;

	public Transform startParent;

	[SerializeField] private GameObject _panel;// эта хреновина нужна, чтоб делать ее вначале перетаскивания родительсиким объектом, 
	//чтоб при отпускании (не на Cell) этот итем не отлетал за границы экрана
	

	private void Start()
	{
		_panel = GameObject.FindGameObjectWithTag("PuzzlePanel");
		canvasGroup = GetComponent<CanvasGroup>();
		startPos = transform.position;
		startParent = transform.parent;
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		transform.SetParent(_panel.transform);
		canvasGroup.alpha = .6f;
		isOncell = false;
		canvasGroup.blocksRaycasts = false;
	}

	public void OnDrag(PointerEventData eventData)
	{
		transform.position = Input.mousePosition;
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		canvasGroup.alpha = 1f;
		canvasGroup.blocksRaycasts = true;

		if (!isOncell)
		{
			transform.SetParent(startParent);
		}
	}

}
