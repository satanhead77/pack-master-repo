﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharactersList : MonoBehaviour
{
	public List<RespawnedCharacter> characters;


	public void AddCharacterToList(RespawnedCharacter character)
	{
		characters.Add(character);
	}

	public void RemoveCharacterFromList(RespawnedCharacter character)
	{
		characters.Remove(character);
	}
}
