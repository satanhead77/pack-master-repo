﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Main : MonoBehaviour
{
	#region fields

	[SerializeField] private AnimatorController _animatorController;
	[SerializeField] private CaseMoveController _caseMoveController;
	[SerializeField] private CharactersMoveController _charactersMoveController;
	[SerializeField] private CharactersList _charactersList;
	[SerializeField] private CharacterGoAway _characterGoAway;
	[SerializeField] private OnTriggerTable _onTriggerTable;
	[SerializeField] private OnTriggerDestroy _onTriggerDestroy;
	[SerializeField] private PuzzleGame _puzzleGame;
	[SerializeField] private DiamondsCounter _diamondsCounter;
	[SerializeField] private ShipsController _shipsController;

	#endregion

	private List<BaseController> _controllersList;

	#region properties

	public static Main Instance { get; private set; }

	public CharactersMoveController CharactersMoveController { get { return _charactersMoveController; } }

	public AnimatorController AnimatorController { get { return _animatorController; } }

	public CaseMoveController CaseMoveController { get { return _caseMoveController; } }

	public CharactersList CharactersList { get { return _charactersList; } }

	public CharacterGoAway CharacterGoAway { get { return _characterGoAway; } }

	public OnTriggerTable OnTriggerTable { get { return _onTriggerTable; } }

	public OnTriggerDestroy OnTriggerDestroy { get { return _onTriggerDestroy; } }

	public PuzzleGame PuzzleGame { get { return _puzzleGame; } }

	public DiamondsCounter DiamondsCounter { get { return _diamondsCounter; } }

	public ShipsController ShipsController { get { return _shipsController; } }

	#endregion

	private void Awake()
	{
		Instance = this;

		_controllersList = new List<BaseController>();

		_controllersList.Add(_charactersMoveController);
		_controllersList.Add(_animatorController);
		_controllersList.Add(_caseMoveController);
		_controllersList.Add(_shipsController);

	}

	private void Update()
	{
		foreach(var controller in _controllersList)
		{
			controller.ControllerUpdate();
		}
	}
}
