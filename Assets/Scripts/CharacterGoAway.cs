﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterGoAway : MonoBehaviour
{
	[SerializeField] private Transform _skipedCharacterMoveTarget;
	[SerializeField] private Transform _acceptedCharacterMoveTarget;
	private RespawnedCharacter character;

	private void Start()
	{
		CanvasManager.Instance.BaggageAcceptanceCanvas.SkipButtonCkicked += SkipCharacter;
		Main.Instance.PuzzleGame.PlayerWin += PlayerWinGoAway;
	}

	private void OnDestroy()
	{
		CanvasManager.Instance.BaggageAcceptanceCanvas.SkipButtonCkicked -= SkipCharacter;
		Main.Instance.PuzzleGame.PlayerWin -= PlayerWinGoAway;
	}

	public RespawnedCharacter GetCharacter()
	{
		return character;
	} 

	private void SkipCharacter()
	{
		GoAway(_skipedCharacterMoveTarget);
		 
	}

	private void PlayerWinGoAway()
	{
		GoAway(_acceptedCharacterMoveTarget);
	}

	private void GoAway(Transform movetarget)
	{
		character = Main.Instance.CharactersList.characters[0];
		character.SetMoveTarget(movetarget);
		Main.Instance.CharactersList.RemoveCharacterFromList(character);
		character.meshAgent.isStopped = false;
		character.meshAgent.SetDestination(movetarget.position);

		foreach (var character in Main.Instance.CharactersList.characters)
		{
			character.SetMoveTarget();
		}
	}
}
